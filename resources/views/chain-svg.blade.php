<svg width="355px" height="408px" viewBox="0 0 355 408" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 47.1 (45422) - http://www.bohemiancoding.com/sketch -->
    <title>Cubes</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Index" transform="translate(-905.000000, -148.000000)">
            <g id="Above-the-fold" transform="translate(148.000000, 75.000000)">
                <g id="Cubes" transform="translate(757.000000, 73.000000)">
                    <path d="M175,69.5852273 L175,171.414773" id="Line-3" stroke="#ccc" stroke-width="8" stroke-linecap="square"></path>
                    <path d="M206.585227,29.5833333 L308.414773,91.4166667" id="Line-4" stroke="#ccc" stroke-width="8" stroke-linecap="square"></path>
                    <path d="M145.414773,29.5865385 L43.5852273,89.4134615" id="Line-5" stroke="#ccc" stroke-width="8" stroke-linecap="square"></path>
                    <path d="M16,144.584158 L16,261.415842" id="Line-6" stroke="#ccc" stroke-width="8" stroke-linecap="square"></path>
                    <path d="M62.5855263,268.418605 L150.414474,219.581395" id="Line-7" stroke="#ccc" stroke-width="8" stroke-linecap="square"></path>
                    <path d="M205.585526,269.418605 L293.414474,220.581395" id="Line-7" stroke="#ccc" stroke-width="8" stroke-linecap="square" transform="translate(249.500000, 245.000000) scale(-1, 1) translate(-249.500000, -245.000000) "></path>
                    <path d="M336,143.588235 L336,262.411765" id="Line-9" stroke="#ccc" stroke-width="8" stroke-linecap="square"></path>
                    <path d="M53.5833333,310.581633 L150.416667,366.418367" id="Line-8" stroke="#ccc" stroke-width="7" stroke-linecap="square"></path>
                    <path d="M205.583333,310.581633 L302.416667,366.418367" id="Line-8" stroke="#ccc" stroke-width="7" stroke-linecap="square" transform="translate(254.000000, 338.500000) scale(-1, 1) translate(-254.000000, -338.500000) "></path>
                    <g id="Cub" transform="translate(142.000000, 170.000000)">
                        <polygon id="Rectangle-10_Left_Isometric" fill="#F9DB1E" points="35 70 0 52.5 0 17.5 35 35"></polygon>
                        <polygon id="Rectangle-10_Right_Isometric" fill="#FE5F55" points="35 70 70 52.5 70 17.5 35 35"></polygon>
                        <polygon id="Rectangle-10_Top_Isometric" fill="#363537" points="0 17.5 35 0 70 17.5 35 35"></polygon>
                    </g>
                    <g id="Cub" transform="translate(4.000000, 251.000000)">
                        <polygon id="Rectangle-10_Left_Isometric" fill="#F9DB1E" points="35 70 0 52.5 0 17.5 35 35"></polygon>
                        <polygon id="Rectangle-10_Right_Isometric" fill="#FE5F55" points="35 70 70 52.5 70 17.5 35 35"></polygon>
                        <polygon id="Rectangle-10_Top_Isometric" fill="#363537" points="0 17.5 35 0 70 17.5 35 35"></polygon>
                    </g>
                    <g id="Cub" transform="translate(142.000000, 338.000000)">
                        <polygon id="Rectangle-10_Left_Isometric" fill="#F9DB1E" points="35 70 0 52.5 0 17.5 35 35"></polygon>
                        <polygon id="Rectangle-10_Right_Isometric" fill="#FE5F55" points="35 70 70 52.5 70 17.5 35 35"></polygon>
                        <polygon id="Rectangle-10_Top_Isometric" fill="#363537" points="0 17.5 35 0 70 17.5 35 35"></polygon>
                    </g>
                    <g id="Cub" transform="translate(285.000000, 252.000000)">
                        <polygon id="Rectangle-10_Left_Isometric" fill="#F9DB1E" points="35 70 0 52.5 0 17.5 35 35"></polygon>
                        <polygon id="Rectangle-10_Right_Isometric" fill="#FE5F55" points="35 70 70 52.5 70 17.5 35 35"></polygon>
                        <polygon id="Rectangle-10_Top_Isometric" fill="#363537" points="0 17.5 35 0 70 17.5 35 35"></polygon>
                    </g>
                    <g id="Cub" transform="translate(285.000000, 86.000000)">
                        <polygon id="Rectangle-10_Left_Isometric" fill="#F9DB1E" points="35 70 0 52.5 0 17.5 35 35"></polygon>
                        <polygon id="Rectangle-10_Right_Isometric" fill="#FE5F55" points="35 70 70 52.5 70 17.5 35 35"></polygon>
                        <polygon id="Rectangle-10_Top_Isometric" fill="#363537" points="0 17.5 35 0 70 17.5 35 35"></polygon>
                    </g>
                    <g id="Cub" transform="translate(142.000000, 0.000000)">
                        <polygon id="Rectangle-10_Left_Isometric" fill="#F9DB1E" points="35 70 0 52.5 0 17.5 35 35"></polygon>
                        <polygon id="Rectangle-10_Right_Isometric" fill="#FE5F55" points="35 70 70 52.5 70 17.5 35 35"></polygon>
                        <polygon id="Rectangle-10_Top_Isometric" fill="#363537" points="0 17.5 35 0 70 17.5 35 35"></polygon>
                    </g>
                    <g id="Cub" transform="translate(0.000000, 86.000000)">
                        <polygon id="Rectangle-10_Left_Isometric" fill="#F9DB1E" points="35 70 0 52.5 0 17.5 35 35"></polygon>
                        <polygon id="Rectangle-10_Right_Isometric" fill="#FE5F55" points="35 70 70 52.5 70 17.5 35 35"></polygon>
                        <polygon id="Rectangle-10_Top_Isometric" fill="#363537" points="0 17.5 35 0 70 17.5 35 35"></polygon>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>